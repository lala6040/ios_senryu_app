//
//  AppDelegate.swift
//  senryu_App
//
//  Created by rkoike on 2020/04/16.
//  Copyright © 2020 rkoike. All rights reserved.
//

import UIKit
import NCMB

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    //********** APIキーの設定 **********
    let applicationkey = "bf30f690b732bde2a14dfa32926a8aeb41d4e5d1a32d14d76285e8b03f7c09ee"
    let clientkey      = "85ee1ad69937711134f1ce71d1f0e8a2c260245a9a720cabd7208472fd407b40"

    internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //********** SDKの初期化 **********
        NCMB.setApplicationKey(applicationkey, clientKey: clientkey)
        //▼▼▼起動時に処理される▼▼▼
        // クラスのNCMBObjectを作成
//        let obj = NCMBObject(className: "TestClass")
//        // オブジェクトに値を設定
//        /** 文字列 **/
//        obj?.setObject("value", forKey: "key")
//        /** 数値 **/
//        obj?.setObject(1, forKey: "number")
//        /** 配列 **/
//        obj?.setObject(["A", "B", "C"], forKey: "array")
//
//        // データストアへの保存を実施
//        obj?.saveInBackground({ (error) in
//            if error != nil {
//                // 保存に失敗した場合の処理
//            }else{
//                // 保存に成功した場合の処理
//            }
//        })
        //▲▲▲起動時に処理される▲▲▲
        return true
    }


    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}



