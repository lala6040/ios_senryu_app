import UIKit
import NCMB

class SignUpViewController: UIViewController {
    
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var button: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    @IBAction func button(_ sender: Any) {
        
        //ユーザー情報を保存
        //NCMBUserのインスタンス
        let user = NCMBUser()
        
        //入力されなかった場合
        if userName.text == "" || password.text == "" {
            return
        }
        
        //ユーザー名を登録
        user.userName = userName.text
        //パスワードを登録
        user.password = password.text
        
        //新規登録する
        user.signUpInBackground { (error) in
            if error != nil {
               // 新規登録失敗時の処理
            } else {
                //ログイン画面へ遷移
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    @IBAction func canselBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
