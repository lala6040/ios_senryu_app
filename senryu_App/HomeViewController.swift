import UIKit
import NCMB

class HomeViewController: UIViewController {
    let userDefaults = UserDefaults.standard
    var memos = [String]()
    
    
    override func viewDidLoad() {
       super.viewDidLoad()
        _ = NCMBUser.current()
    }
    
    @IBAction func signOut(_ sender: Any) {
        NCMBUser.logOut()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func toPostBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "toPost", sender: nil)
    }
    
    @IBAction func allPostBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "allPost", sender: nil)
    }
}
