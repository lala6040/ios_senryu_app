import UIKit
import NCMB

class SignInViewController: UIViewController {
    
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var login: UIButton!
    
    
     override func viewDidLoad() {
       super.viewDidLoad()
    }
    
    @IBAction func login(_ sender: Any) {
        if userName.text!.isEmpty || password.text!.isEmpty {
               return
           }

        NCMBUser.logInWithUsername(inBackground: userName.text, password: password.text) { (result,error) in
            if error != nil {
                print("error")
            } else {
                //遷移
                self.performSegue(withIdentifier: "success", sender: nil)
                
            }
        }
    }
    @IBAction func signUpBtn(_ sender: Any) {
        
         self.performSegue(withIdentifier: "toSignUp", sender: nil)
    }
    
   
}
