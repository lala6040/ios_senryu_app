import UIKit
import NCMB

class ToPostViewController: UIViewController {
    
    var memo: String?
    @IBOutlet weak var Text1: UITextField!
    @IBOutlet weak var Text2: UITextField!
    @IBOutlet weak var Text3: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
       super.viewDidLoad()
        if let memo = self.memo {
            self.Text1.text = memo
            self.Text2.text = memo
            self.Text3.text = memo
        }
        self.updateSaveButtonState()
    }
    
    private func updateSaveButtonState() {
       let memo = self.Text1.text ?? ""
        _ = self.Text2.text ?? ""
        _ = self.Text3.text ?? ""
       self.saveButton.isEnabled = !memo.isEmpty
       self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.memo = self.Text1.text ?? ""
        self.memo = self.Text2.text ?? ""
        self.memo = self.Text3.text ?? ""
    }
    @IBAction func cancelBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
